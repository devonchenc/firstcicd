#pragma once

class Factorial
{
    public:
    static unsigned long long GetFactorial(int input);
};
